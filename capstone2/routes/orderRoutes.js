const auth = require("../auth.js");
const express = require("express");
const router = express.Router();

const orderControllers = require("../controllers/orderControllers.js");

// Routes


router.get("/", auth.verify, orderControllers.getAllOrders);

module.exports = router;