const auth = require("../auth.js");
const mongoose = require("mongoose");

const User = require("../models/User.js");
const Product = require("../models/Product.js");
const Cart = require("../models/Cart.js");
const Order = require("../models/Order.js");





module.exports.addToCart = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const userId = userData.id;
  const productId = request.params.productId;
  const { quantity } = request.body;

  let cart = await Cart.findOne({ userId });

  if (!cart) {
    cart = new Cart({
      userId,
      products: [],
      totalAmount: 0,
    });
  }

  const product = await Product.findById(productId);

  const existingProductIndex = cart.products.findIndex(
    (product) => product.productId === productId
  );

  if (existingProductIndex !== -1) {

    response.send(false);
  } else {

    const subTotal = product.price * quantity;
    cart.products.push({
      productId,
      quantity,
      subTotal,
    });

    cart.totalAmount = cart.products.reduce(
      (total, product) => total + product.subTotal,
      0
    );

    await cart.save();
    response.send(cart);
  }
};



module.exports.userCart = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const userId = userData.id;

  return Cart.findOne({ userId })
    .then((cart) => {
      if (cart) {
        const productArray = cart.products.map((product) => ({
          productId: product.productId,
          subTotal: product.subTotal
        }));

        const cartData = {
          products: productArray,
          totalAmount: cart.totalAmount
        };

        response.send(cartData);
      } else {
        response.send('No cart for user yet! Please add a product');
      }
    })
    .catch((error) => response.send(error));
};

module.exports.deleteProductFromCart = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const userId = userData.id;
  const productId = request.params.productId;

  try {
    const cart = await Cart.findOne({ userId });

    if (!cart) {
      return response.status(404).send("Cart not found");
    }

    const productIndex = cart.products.findIndex(
      (product) => product.productId === productId
    );

    if (productIndex === -1) {
      return response.status(404).send("Product not found in cart");
    }

    const product = cart.products[productIndex];

      cart.products.splice(productIndex, 1);
      cart.totalAmount = cart.products.reduce(
        (total, product) => total + product.subTotal,
        0
      );
      await cart.save();
      return response.send("Product removed from cart");

  } catch (error) {
    console.log("Error deleting product from cart:", error);
    return response.status(500).send("Internal server error");
  }
};