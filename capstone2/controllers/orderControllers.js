const auth = require("../auth.js")

const User = require("../models/User.js")
const Product = require("../models/Product.js")
const Cart = require("../models/Cart.js")
const Order = require("../models/Order.js")

module.exports.createOrder = async (request, response) => {
  const cartProductIds = request.body.id

  const userData = auth.decode(request.headers.authorization)

  const userId = userData.id

    const cart = await Cart.findOne({userId})

      const orderProducts = []
        let totalAmount = 0

        for(const product of cart.products) {
          if(cartProductIds && !cartProductIds.includes(product.productId)) {
            continue
          }

          orderProducts.push({
            productId: product.productId,
            quantity: product.quantity,
          })

          totalAmount += product.subTotal
        }

        if(orderProducts.length === 0) {

          return response.send("Please select a product from your cart")
          
        }

        const newOrder = new Order({
          userId: userData.id,
          products: orderProducts,
          totalAmount: totalAmount,
        })

        await newOrder.save()
        .then(async result => {
          const order = {
            orderNumber: newOrder._id,
            userId: userData.id,
            products: orderProducts,
            totalAmount: totalAmount,
          }

          if(cartProductIds) {

            await Cart.updateOne({userId}, {$pull:{products:{productId:{$in: cartProductIds}}}})

          }else {

            await Cart.updateOne({userId}, {$set:{products:[]}})

          }

          cart.totalAmount = 0;
          await cart.save()
          .catch(error => (error))

          response.send(order)
        })
        .catch(error => (error))
}


module.exports.getAllOrders = (request,response) => {

	const userData = auth.decode(request.headers.authorization)

	if(userData.isAdmin){
    Order.find({})
    .then(result => response.send(result))
    .catch(error => response.send(error))
 }else {
  return response.send(`You do not have access to this route.`)
}
}
