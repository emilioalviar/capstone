const auth = require('../auth.js');
const bcrypt = require("bcrypt");

const User = require("../models/User.js");
const Product = require("../models/Product.js")
const Cart = require("../models/Cart.js")
const Order = require("../models/Order.js")

module.exports.registerUser = (request, response) => {

	User.findOne({email: request.body.email})
	.then(result => {

		if(result) {

			return response.send(false)

		}else{

			let newUser = new User({
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
				address: request.body.address,
				password: bcrypt.hashSync(request.body.password, 10),
				isAdmin: request.body.isAdmin,
			})

			newUser.save()
			.then(saved => {

					let cart = new Cart({
						userId: saved._id,
						products: [],
						totalAmount: 0,
					})

					cart.save()
					.then(final => {
						response.send(true)
					})
					.catch(error => response.send(error));

			})
			.catch(error => response.send(error));
		}
	})
	.catch(error => response.send(error));
}


module.exports.loginUser = (request,response) => {

	User.findOne({email: request.body.email})
	.then(result => {

		if(!result){

			return response.send(false)

		}else{

			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password)

			if(isPasswordCorrect){

				return response.send({
					auth: auth.createAccessToken(result)
				})

			}else{

				return response.send(false)

			}
		}
	})
	.catch(error => response.send(error));
}

module.exports.userDetails = async (request, response) => {

  const userId = request.params.userId;
  const userData = auth.decode(request.headers.authorization);

  if(userData.isAdmin) {

  	const user = await User.findById(userId);

  	if(user) {

  	  user.password = "*************"

  	  const orders = await Order.find({ userId: userId });
  	  const userDetails = {
  	    user: user,
  	    orders: orders,
  	  }

  	  return response.send(userDetails)

  	}else{

  	  return response.send(`User not found.`)

  	}

  }else{

  	if(userData.id !== userId) {

  	  return response.send(false)

  	}else {

  		const user = await User.findById(userId);

  		if(user) {

  		  user.password = "*************"

  		  const orders = await Order.find({ userId: userId });
  		  const userDetails = {
  		    user: user,
  		    orders: orders,
  		  }

  		  return response.send(userDetails)

  		}else {

  		  return response.send(`User not found.`)
  		}
  	}
  }
}

module.exports.retrieveUserData = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	User.findOne({_id : userData.id})
	.then(data => response.send(data));
}

module.exports.retrieveUserDataforOrder = (request, response) => {
	const userData = request.body.userId;

	User.findOne({_id : userData})
	.then(data => response.send(data));
}


module.exports.userAdmin = (request, response) => {

	const userData = auth.decode(request.headers.authorization)
	const userId = request.body.id

	if (userData.isAdmin) {

		User.findById(userId)
		.then(result => {

			if(!result) {

				return response.send(`User not found`)

			}else{

				if (result.isAdmin) {

					return response.send(`User ${userId} is already an admin`)

				} else {

					const userAdmin = {
						isAdmin: true,

					}

					User.findByIdAndUpdate(userId, userAdmin)
					.then(result => {

						Cart.findOneAndRemove({userId})
						.then(result => {

							response.send(`User ${userId} is now an Admin! Deleted cart!`)

						})
						.catch(error => response.send(error))
					})
					.catch(error => response.send(error))
				}
			}
		})
		.catch(error => response.send(error))
	}else{
		return response.send(`You don't have access to this route.`)
	}
}



module.exports.userOrders = async (request, response) => {

	const userData = auth.decode(request.headers.authorization)
	const userId = userData.id

	await User.findById(userId)
	.then(result => {

		Order.find({userId : userId})
		.then(result => {

			if(result){

				return response.send(result)

			}else{

				return response.send(false)

			}
		})
		.catch(error => response.send(error))
	})
	.catch(error => response.send(error))
}





