import React, { useState, useEffect} from 'react';
import { useParams } from 'react-router-dom';
import { Container, Row } from 'react-bootstrap';
import BrandProductCard from '../components/BrandProductCard';

export default function BrandView() {
  const [products, setProducts] = useState([]);
  const { brandId } = useParams();
  const decodedBrandId = decodeURI(brandId)

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/brand/${decodedBrandId}`)
      .then((result) => result.json())
      .then((data) => {
        const activeProducts = data.filter((product) => product.isActive);
        setProducts(activeProducts);
      })
      .catch((error) => {
        console.log('Error fetching products:', error);
      });
  }, []);
 
  return (
    <Container>
      <Row>
        {products.map((product) => (
          <BrandProductCard key={product._id} productProp={product} />
        ))}
      </Row>
    </Container>
  );
}
