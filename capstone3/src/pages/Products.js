import React from 'react';
import { useNavigate, Link } from 'react-router-dom';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';

export default function Products() {

  const brands = [
    { id: 'Rolex', title: 'Rolex', imageUrl: 'https://www.watchpro.com/2021/10/Rolex-clasp.jpg' },
    { id: 'Patek Philippe', title: 'Patek Philippe', imageUrl: 'https://images.augustman.com/wp-content/uploads/sites/3/2022/11/03134845/Grand-Complications-Celestial-6102.jpg' },
    { id: 'Audemars Piguet', title: 'Audemars Piguet', imageUrl: 'https://www.christies.com/img/LotImages/2023/DUB/2023_DUB_21993_0049_000(audemars_piguet_royal_oak_ref_25654ba_a_gold_automatic_perpetual_calen122457).jpg?mode=max?w=780' },
    { id: 'Jaeger LeCoultre', title: 'Jaeger LeCoultre', imageUrl: 'https://monochrome-watches.com/wp-content/uploads/2019/10/Jaeger-LeCoultre-Master-Grande-Tradition-GyroTourbillon-3-Meteorite-8.jpg' },
  ];


  return (
    <Container>
      <Row>
        {brands.map((brand) => (
          <Col key={brand.id}>
            <div className="d-flex justify-content-center align-items-center h-100">
              <Link to={`/products/brand/${brand.id}`} className="banner-link">
                <Card className="card-landing">
                  <div className="card-image">
                    <Card.Img variant="top" src={brand.imageUrl} alt={brand.title} />
                    <div className="card-text-overlay">
                      <span>{brand.title}</span>
                    </div>
                  </div>
                </Card>
              </Link>
            </div>
          </Col>
        ))}
      </Row>
      <Row>
        <div className="d-flex justify-content-center w-100">
          <Link to="/allProducts" className="custom-link">
            <p>Didn't find what you were looking for?</p>
          </Link>
        </div>
      </Row>
    </Container>
  );
}
