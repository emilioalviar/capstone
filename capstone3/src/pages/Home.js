import Banner from '../components/Banner.js';

export default function Home() {
  return (
    <div style={{
      backgroundColor: 'white',
      backgroundImage: `url("https://wallpaperaccess.com/full/812847.jpg")`,
      height: '90vh',
      backgroundSize: 'cover',
      position: 'relative',
    }}>
      <div
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
          height: '100%',
          backgroundColor: 'rgba(0, 0, 0, 0.5)',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Banner />
      </div>
    </div>
  );
}
