import { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import Swal2 from 'sweetalert2';
import { Container, Row, Col, Button, Card } from 'react-bootstrap';
import UserContext from '../UserContext.js';

export default function ProductView() {
  const [brand, setBrand] = useState('');
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');

  const { user } = useContext(UserContext);
  const { productId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((result) => result.json())
      .then((data) => {
        setBrand(data.brand);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, []);

  const handleAddToCart = () => {
    if (user) {
      if (user.isAdmin === true) {

        Swal2.fire({
            title: 'Admin cannot add to Cart!',
            icon: 'error',
            showConfirmButton: false,
        });

      } else {

      	fetch(`${process.env.REACT_APP_API_URL}/user/${productId}/cart`, {
      	  method: 'PATCH',
      	  headers: {
      	    'Authorization': `Bearer ${localStorage.getItem('token')}`,
      	    'Content-Type': 'application/json',
      	  },
      	  body: JSON.stringify({ quantity: 1 }),
      	})
      	  .then((response) => response.json())
      	  .then((data) => {
      	  	if(data === false){
      	  		Swal2.fire({
      	  		    title: 'Product not added to Cart!',
      	  		    icon: 'error',
      	  		    text: 'Product already in cart!',
                  showConfirmButton: false,
      	  		})
      	  	}else{
      	  		Swal2.fire({
      	  		    title : 'Product added to Cart!',
      	  		    icon : 'success',
                  showConfirmButton: false,
      	  		})
      	  	}
      	  })
      	  .catch((error) => {
      	    console.error('Error adding product to cart:', error);
      	  });

      }
    } else {
      Swal2.fire({
          title: 'Please Log In first!',
          icon: 'error',
          showConfirmButton: false,
      });
      navigate('/login');
    }
  };

  return (
    <Container>
      <Row>
        <Col>
          <Card className="border border-black">
            <Card.Body>
              <Card.Title>
                {brand} {name}
              </Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Text>Price: Php {price.toLocaleString()}</Card.Text>
              <Button variant="dark" className="w-100" onClick={handleAddToCart}>
                Add To Cart
              </Button>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
