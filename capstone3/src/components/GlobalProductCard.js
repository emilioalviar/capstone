import { Container, Row, Col, Card, Button, Modal, Form } from 'react-bootstrap';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import Swal2 from 'sweetalert2';

export default function GlobalProductCard(props) {
	const {_id} = props.productProp;

	const [brand, setBrand] = useState(props.productProp.brand)
	const [name, setName] = useState(props.productProp.name)
	const [description, setDescription] = useState(props.productProp.description)
	const [price, setPrice] = useState(props.productProp.price)
	const [isActive, setIsActive] = useState(props.productProp.isActive);


	return (
	  <Col xs={12} className="mb-3">
		  <Link to={`/products/${_id}`} className="text-decoration-none">
		    <Card className="global-product-card-custom border border-black">
		      <Card.Body className="global-product-card-body">
		        <Card.Title className="global-product-name-custom">{brand} {name}</Card.Title>
		      </Card.Body>
		    </Card>
		  </Link>
	  </Col>
	);
}
