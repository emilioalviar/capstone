import { useState, useEffect, useContext } from 'react';
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import { FiMenu, FiUser } from 'react-icons/fi';

import UserContext from '../UserContext.js';

export default function AppNavBar() {
  const [isMobile, setIsMobile] = useState(false);

  const checkMobileView = () => {
    setIsMobile(window.innerWidth <= 767);
  };

  useEffect(() => {
    window.addEventListener('resize', checkMobileView);
    return () => {
      window.removeEventListener('resize', checkMobileView);
    };
  }, []);

  const { user } = useContext(UserContext);

  return (
    <Navbar variant="light" className='navbar-custom'>
      <Container>
        {isMobile ? (
          <>
            <Nav className="navbar-mobile-menu-custom">
              <NavDropdown title={<FiMenu />} id="navbar-dropdown">
                <NavDropdown.Item as={NavLink} to="/aboutUs" exact>
                  About Us
                </NavDropdown.Item>
                <NavDropdown.Item as={NavLink} to="/products">
                  Products
                </NavDropdown.Item>
                <NavDropdown.Item as={NavLink} to="/contactUs">
                  Contact Us
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>

            <Navbar.Brand as={Link} to="/" className="custom-navbar-brand">
              Watch Couture
            </Navbar.Brand>

            {user === null ? (
              <Nav className="navbar-mobile-account-custom">
                <Nav.Link as={NavLink} to="/login">
                  <FiUser />
                </Nav.Link>
              </Nav>
            ) : (
            <Nav className="navbar-mobile-account-custom">
              <NavDropdown title={<FiUser />} id="navbar-dropdown">
                {user.isAdmin ? (
                  <>
                    <NavDropdown.Item as={NavLink} to="/adminDashboard">
                      Admin Dashboard
                    </NavDropdown.Item>
                    <NavDropdown.Item as={NavLink} to="/logout">
                      Logout
                    </NavDropdown.Item>
                  </>
                ) : (
                  <>
                    <NavDropdown.Item as={NavLink} to="/cart">
                      Cart
                    </NavDropdown.Item>
                    <NavDropdown.Item as={NavLink} to="/orders" exact>
                      Orders
                    </NavDropdown.Item>
                    <NavDropdown.Item as={NavLink} to="/logout">
                      Logout
                    </NavDropdown.Item>
                  </>
                )}
              </NavDropdown>
            </Nav>
            )}
          </>
        ) : (
          <Nav className="navbar-desktop-custom">
            <Nav.Link as={NavLink} to="/aboutUs">
              About Us
            </Nav.Link>
            <Nav.Link as={NavLink} to="/products" className="navbar-desktop-products-custom">
              Products
            </Nav.Link>
            <div className="d-flex justify-content-center align-items-center flex-grow-1">
              <Navbar.Brand as={Link} to="/" className="custom-navbar-brand">
                Watch Couture
              </Navbar.Brand>
            </div>
            <Nav className="justify-content-end">
              <Nav.Link as={NavLink} to="/contactUs" className="navbar-desktop-contact-custom">
                Contact Us
              </Nav.Link>
              {user === null ? (
                <Nav className="ms-auto">
                  <Nav.Link as={NavLink} to="/login">
                    <FiUser />
                  </Nav.Link>
                </Nav>
              ) : (
              <NavDropdown title={<FiUser />} id="navbar-dropdown">
                {user.isAdmin ? (
                  <>
                    <NavDropdown.Item as={NavLink} to="/adminDashboard">
                      Admin Dashboard
                    </NavDropdown.Item>
                    <NavDropdown.Item as={NavLink} to="/logout">
                      Logout
                    </NavDropdown.Item>
                  </>
                ) : (
                  <>
                    <NavDropdown.Item as={NavLink} to="/cart">
                      Cart
                    </NavDropdown.Item>
                    <NavDropdown.Item as={NavLink} to="/orders" exact>
                      Orders
                    </NavDropdown.Item>
                    <NavDropdown.Item as={NavLink} to="/logout">
                      Logout
                    </NavDropdown.Item>
                  </>
                )}
              </NavDropdown>
              )}
            </Nav>
          </Nav>
        )}
      </Container>
    </Navbar>
  );
}
