import { Container, Row, Col, Card, Button, Modal, Form } from 'react-bootstrap';
import { useState } from 'react';
import Swal2 from 'sweetalert2';

export default function AdminProductCard(props) {
	const [isModalOpen, setIsModalOpen] = useState(false);
	const {_id} = props.productProp;

	const [brand, setBrand] =useState(props.productProp.brand)
	const [name, setName] = useState(props.productProp.name)
	const [description, setDescription] = useState(props.productProp.description)
	const [price, setPrice] = useState(props.productProp.price)
	const [isActive, setIsActive] = useState(props.productProp.isActive);

	const openModal = () => {
		setIsModalOpen(true);
	};

	const closeModal = () => {
		setIsModalOpen(false);
	};

	const handleUpdate = (event) => {
		event.preventDefault();

		const updatedProduct = {
			brand: event.target.brand.value,
			name: event.target.name.value,
			description: event.target.description.	
			value,
			price: event.target.price.value,
		};

		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`, {
			method: 'PATCH',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(updatedProduct),
		})
		.then((response) => response.json())
		.then((data) => {
			if(data === false){
				Swal2.fire({
					title: 'Product name already in use!',
					text: 'An error occurred while updating the product.',
					icon: 'error',
					showConfirmButton: false,
				})
			}else{
				setBrand(updatedProduct.brand);
				setName(updatedProduct.name);
				setDescription(updatedProduct.description);
				setPrice(updatedProduct.price);
				Swal2.fire({
					title: 'Product Updated',
					text: `${name}`,
					icon: 'success',
					showConfirmButton: false,
				});
				setIsModalOpen(false);
			}
		})
		.catch((error) => {
			Swal2.fire({
				title: 'Error',
				text: 'An error occurred while updating the product.',
				icon: 'error',
				showConfirmButton: false,
			});
			console.log(error);
		});
	};
	
	const archiveProduct = () => {
		if (isActive === false) {
			alert(`${name} is already archived!`);
		} else {
			fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
				method: 'PATCH',
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`,
					'Content-Type': 'application/json',
				},
			})
			.then((data) => {
				setIsActive(false)
				Swal2.fire({
					title: 'Product Archived',
					text: `${name}`,
					icon: 'success',
					showConfirmButton: false,
				});
			})
			.catch((error) => {
				Swal2.fire({
					title: 'Error',
					text: 'An error occurred while archiving the product.',
					icon: 'error',
					showConfirmButton: false,
				});
				console.log(error);
			});
		}
	};

	const activateProduct = () => {
		if(isActive === true){
			alert(`${name} is already active!`)
		}else{
			fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/activate`, {
				method: 'PATCH',
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`,
					'Content-Type': 'application/json',
				},
			})
			.then((result) => result.json())
			.then((data) => {
				setIsActive(true)
				Swal2.fire({
					title: 'Product Activated',
					text: `${name}`,
					icon: 'success',
					showConfirmButton: false,
				});
			})
			.catch((error) => {
				Swal2.fire({
					title: 'Error',
					text: 'An error occurred while activating the product.',
					icon: 'error',
					showConfirmButton: false,
				});
				console.log(error);
			});
		}
	};

	return (
		<Container>
			<Row>
				<Col className="mt-3">
					<Card>
						<Card.Body className="border border-black">
							<Card.Title>{brand} {name}</Card.Title>
							<Card.Subtitle className="mb-1">Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price.toLocaleString()}</Card.Text>
							<Card.Subtitle>Is Active:</Card.Subtitle>
							<Card.Text>{isActive ? 'Active' : 'Inactive'}</Card.Text>

							<Button className="mt-0" variant="light" style={{backgroundColor: 'black', color: 'white',width: '175px',borderRadius: '5px',padding: '15px 15px',fontSize: '1rem',margin: '0px',}}onClick={openModal}>Update Product</Button>
							<Button className="mt-0" variant="light" style={{ backgroundColor: 'black', color: 'white', width: '175px', borderRadius: '5px', padding: '15px 15px', fontSize: '1rem', margin: '0px'}} onClick={isActive ? archiveProduct : activateProduct} > {isActive ? 'Archive' : 'Activate'} </Button>
						</Card.Body>
					</Card>
				</Col>
			</Row>

		<Modal show={isModalOpen} onHide={closeModal}>
			<Modal.Header closeButton>
				<Modal.Title>Update Product</Modal.Title>
			</Modal.Header>

			<Modal.Body>
				<Form onSubmit={handleUpdate}>
					<Form.Group controlId="formBrand">
						<Form.Label>Brand</Form.Label>
						<Form.Control type="text" name="brand" defaultValue={brand} />
					</Form.Group>
					<Form.Group controlId="formName">
						<Form.Label>Name</Form.Label>
						<Form.Control type="text" name="name" defaultValue={name} />
					</Form.Group>
					<Form.Group controlId="formDescription">
						<Form.Label>Description</Form.Label>
						<Form.Control as="textarea" name="description" defaultValue={description} />
					</Form.Group>
					<Form.Group controlId="formPrice">
						<Form.Label>Price</Form.Label>
						<Form.Control type="text" name="price" defaultValue={price} />
					</Form.Group>

					<Button variant="dark" type="submit" className="mt-5">
					Save Changes
					</Button>
				</Form>
			</Modal.Body>
		</Modal>

		</Container>
		);
}
