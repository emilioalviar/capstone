import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import './App.css';

import {useState, useEffect} from 'react';

import AppNavBar from './components/AppNavBar.js';
import Home from './pages/Home.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import AdminDash from './pages/AdminDash.js';
import Logout from './pages/Logout.js';
import AllProducts from './pages/AllProducts.js';
import ProductView from './pages/ProductView.js';
import Products from './pages/Products.js';
import BrandView from './pages/BrandView.js';
import AboutUs from './pages/AboutUs.js';
import ContactUs from './pages/ContactUs.js';
import Cart from './pages/Cart.js'
import Orders from './pages/Orders.js'
import AdminAllOrders from './pages/AdminAllOrders.js'


import {UserProvider} from './UserContext.js';

import 'bootstrap/dist/css/bootstrap.min.css';

import {BrowserRouter, Route, Routes} from 'react-router-dom';

function App() {
  const [user, setUser] = useState(null);

  useEffect(() => {
    if (localStorage.getItem('token') !== null) {
      fetch(`${process.env.REACT_APP_API_URL}/user/userData`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
        .then(result => result.json())
        .then(data => {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        })
        .catch(error => {
          console.log('Error fetching user data:', error);
        });
    }
  }, []);

  const unsetUser = () => {
    localStorage.clear();
    setUser(null);
  };

  return (

    <UserProvider value={{ user, setUser, unsetUser }}>
      <BrowserRouter>
        <AppNavBar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/adminDashboard" element={<AdminDash />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/Products" element={<Products />} />
          <Route path="/allProducts" element={<AllProducts />} />
          <Route path="/aboutUs" element={<AboutUs />} />
          <Route path="/contactUs" element={<ContactUs />} />
          <Route path = '/products/brand/:brandId' element = {<BrandView />} />
          <Route path = '/products/:productId' element = {<ProductView />} />
          <Route path = '/cart' element = {<Cart />} />
          <Route path = '/orders' element = {<Orders />} /> 
          <Route path = '/allOrders' element ={<AdminAllOrders />} />
        </Routes>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
